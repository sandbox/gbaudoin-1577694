README.txt
==========

This module deletes unpublished nodes at cron run.

Options available:

    1. Cron run frequency
    2. How old should be the nodes
    3. From witch content type should be the nodes
    4. How many unpublished nodes to delete per content type per cron run.


AUTHOR/MAINTAINER
======================
Florin Munteanu <idflorin at gmail DOT com> http://nodeid.com

